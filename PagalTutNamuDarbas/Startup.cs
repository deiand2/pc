﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PagalTutNamuDarbas.Startup))]
namespace PagalTutNamuDarbas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
