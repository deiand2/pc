﻿CREATE TABLE [dbo].[Companies] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [CompanyName]    NVARCHAR (MAX) NULL,
    [NumbEmployee]   INT            NULL,
    [NumbCompetence] INT            NULL,
    [Description]    NVARCHAR (MAX) NULL,
    [Date]           DATETIME       NULL,
    CONSTRAINT [PK_dbo.Companies] PRIMARY KEY CLUSTERED ([ID] ASC)
);

