﻿using System;
using System.Data.Entity;

namespace PagalTutNamuDarbas.Models
{
    public class Evaluation
    {
            public int ID { get; set; }
            public string UserID { get; set; }
            public string CompetenceID { get; set; }
            public int Evaluate { get; set; }
            public DateTime DateEval { get; set; }
        }
        public class EvaluationDBContext : DbContext
        {
            public DbSet<Evaluation> Evaluations { get; set; }
        }
}