﻿using System;
using System.Data.Entity;

namespace PagalTutNamuDarbas.Models
{
    public class Competence
    {
            public int ID { get; set; }
            public string CompetenceName { get; set; }
            public string Status { get; set; }

        }
        public class CompetenceDBContext : DbContext
        {
            public DbSet<Competence> Competencies { get; set; }
        }

}