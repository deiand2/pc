﻿using System;
using System.Data.Entity;

namespace PagalTutNamuDarbas.Models
{
    public class Company
    {
        public int ID { get; set; }
        public string CompanyName { get; set; }
        public int NumbEmployee { get; set; }
        public int NumbCompetence { get; set; }
        
    }
    public class CompanyDBContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
    }
}