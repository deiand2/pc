﻿using System.Data.Entity;

namespace PagalTutNamuDarbas.Models
{
    public class Worker
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

    }
    public class WorkerDBContext : DbContext
    {
        public DbSet<Worker> Workers { get; set; }
    }
}